# v3.0.2

  * MSRV update to 1.49 due to dependencies.

# v3.0.1

## Changes

  * There is now a `Director::watch_directory_in` `async` method which can be
    used when providing an appropriate runtime from the caller.

## Fixes

  * The `inotify` watch is not ordered before the "list existing jobs" part to
    avoid missing jobs that show up in between.

# v3.0.0

## Breaking changes

  * There is now a `HandlerCore` trait which provides the `add_to_director`
    method.
  * The `Director` now runs registered handlers asynchronously.
  * The `Handler` trait is now `async` since the `Director` only handles
    `async` handlers anyways.
  * MSRV bumped to 1.47. Some of the `async` implementation details use new
    features introduced in newer Rust releases (here, generic array bounds).
  * `HandlerResult` is now `JobResult` since it is the result of a job, not a
    handler.

## Changes

  * `Handler` now works with `JobError` rather than `Box<dyn Error>` (as an
    alias) to make reading the code much easier.

# v2.0.1

  * Error types are now expected to be `Send` and `Sync` as well.
  * Added `HandlerResult::accept()`, `HandlerResult::restart()`, and
    `HandlerResult::done()`.

# v2.0.0

  * `error-chain` has been removed in favor of custom error types using
    `thiserror`.
  * `utils::archive_queue` safety has improved. Errors while archiving should
    no longer lose jobs.
  * `Director` now accepts an iterator of paths to run.
  * Add `defer()`, `reject()`, and `fail()` constructor methods to
    `HandlerResult`. These take arguments by traits to avoid unnecessary calls
    to `Box::new()` and `.into()` in callers

# v1.2.0

  * Fix test suite to not rely on `io::Error`'s `Debug` output.
  * Add `utils::drop_job`.

# v1.1.2

  * Dependency specifications for `serde_json`.

# v1.1.1

  * Dependency specifications for `git-workarea` and `error-chain` have been
    relaxed.

# v1.1.0

  * `utils::write_job` now waits for the job file to have been processed
    before returning.
  * At least `serde_json-0.9.8` is now required for the `entry` method.

# v1.0.1

  * Archives are now written using RFC3339. This removes the spaces that were
    previously there.
  * Jobs are added to the archive properly now as well.

# v1.0.0

  * Initial stable release.
